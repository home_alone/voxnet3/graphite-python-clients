import random
import datetime
import graphyte  # pip install graphyte
import robgracli  # pip install robust-graphite-client
import time


GRAPHITE_HOST = '127.0.0.1'

client = None


def init():
    graphyte.init(GRAPHITE_HOST)
    global client
    client = robgracli.GraphiteClient('http://{}'.format(GRAPHITE_HOST))


def simple_poc():
    graphyte.send('vox.poc', 666)

    results = run_query('vox.*', from_=10)  # from_ - in seconds

    assert results['vox.poc'][-1][0] == 666
    print 'simple POC works'


def send_series(series_name, points_generator, start_time, time_interval):
    print 'Sending {}'.format(series_name)
    now = time.time()
    for i, point in enumerate(points_generator):
        point_time = start_time + time_interval * i
        if point_time > now:
            return
        graphyte.send(series_name, point, timestamp=point_time)


def run_query(query, from_=None, until=None):
    # from_, until - in seconds
    # `robgracli` is pretty shitty because it has no "until" parameter.
    # We can either patch it or just use requests. the REST API is pretty damn simple...
    results = client.query(query, from_=from_)
    return {key: sorted(points, key=lambda (p, t): t) for key, points in results.items()}


def average_usage_poc():
    start_time = time.time() - 60 * 60 * 5
    time_interval = 10

    send_series('vox.usage.always_low', _uniform(200, 600), start_time, time_interval)
    send_series('vox.usage.always_high', _uniform(10000, 30000), start_time, time_interval)
    send_series('vox.usage.low_with_peaks', _usually_low_but_has_peaks(), start_time, time_interval)

    results = run_query("vox.usage.* | movingAverage('10min') | aliasByNode(2)", from_=60 * 60)
    for name, points in results.items():
        print '@@@ {} @@@'.format(name)
        for val, timestamp in points[-10:]:
            print '{}\t{}'.format(datetime.datetime.fromtimestamp(timestamp), val)


def _uniform(lower_bound, upper_bound):
    while True:
        yield random.randint(lower_bound, upper_bound)


def _usually_low_but_has_peaks():
    high = _uniform(20000, 50000)
    low = _uniform(100, 300)
    while True:
        should_peak = random.random() < 0.005
        if should_peak:
            peak_length = random.randint(3, 10)
            for _ in xrange(peak_length):
                yield next(high)
        else:
            yield next(low)


def main():
    init()
    simple_poc()
    average_usage_poc()


if __name__ == '__main__':
    main()
